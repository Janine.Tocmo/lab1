package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multipliedWithTwo(null);
        removeThrees(null);

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        // DYNAMISK LISTE (forelesning)
        // ArrayList<Integer> myList = new ArrayList<>();
        // quick fix, import ArrayList
        // myList.add(e:5);
        // System.out.println(myList);
        // int num = myList.get(index:2);
        // myList.set(num)

        ArrayList<Integer> multipliedList = new ArrayList<>(); {

            for (int num : list) {
                multipliedList.add(num * 2);
            }

            return multipliedList;

            // for (int i = 0; i < multipliedList.size(); i++) {
                // i = i * 2;
                // System.out.println(multipliedList.get(i));

                // List<Integer> myResults = new ArrayList<>();
                
                // multipliedList.addAll(multipliedList)

        }
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> removedThrees = new ArrayList<>();

        for (int num : list) {
            if (num != 3) {
                removedThrees.add(num);
            }

        }
        return removedThrees;

    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueNumbers = new ArrayList<>();

        for (int element : list) {
            if (!uniqueNumbers.contains(element))
            uniqueNumbers.add(element);
        }
        return uniqueNumbers;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, b.get(i) + a.get(i));
        }

        // void skal ikke returnere noe
    }

}