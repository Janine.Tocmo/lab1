package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
       int sumRow = 0;
       int checkRow = 0;

        for (int i = 0; i < grid.size(); i++) {
            for ( int j = 0; j < grid.get(0).size(); j++) {
                checkRow += grid.get(0).get(j);
                sumRow += grid.get(i).get(j);
                
            }

            if (checkRow != sumRow) {
                return false;
            }
            
            sumRow = 0;
            checkRow = 0;
        }

        int checkCol = 0;
        int sumCol = 0;

         for (int i = 0; i < grid.size(); i++) {
             for ( int j = 0; j < grid.get(0).size(); j++) {
                 checkCol += grid.get(j).get(0);
                 sumCol += grid.get(j).get(i);
                 
             }
 
             if (checkCol != sumCol) {
                 return false;
             }
             
             sumCol = 0;
             checkCol = 0;        
        }
        return true;
    }
}

// // OG:
// int rowSum = sumRow(grid, i:0);
// for (int row = 1; row < grid.size(); row++) {
//     if (sumRow(grid, row) != rowSum) {
//         return false;
//     }
// }

// int colSum = sumCol(grid, i:0);
// for (int col = 0; col < grid.size(); col++) {
//     if (sumCol (grid, col) != colSum) {
//         return false;
//     }
// }
// return true;