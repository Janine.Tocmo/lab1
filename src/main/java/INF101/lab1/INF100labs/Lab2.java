package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords(null, null, null);
        isLeapYear(0);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLen = 0;

        // Eller slik (fra oppgavegjennomgang):
        // int maxLen = Integer.max(word1.length(), word2.length());
        // maxLen = Integer.max(maxLen, word3.length());
        // if (word1.length() == maxLen) {
        //     System.out.println(word1);
        // }

        // if (word2.length() == maxLen) {
        //     System.out.println(word2);
        // }

        // if (word3.length() == maxLen) {
        //     System.out.println(word3);
        // }

        int lengthFirst  = word1.length();
        int lengthSecond = word2.length();
        int lengthThird = word3.length();

        if (lengthFirst > maxLen) {
            maxLen = lengthFirst;
        } 
        if (lengthSecond > maxLen) {
            maxLen = lengthSecond;
        }
        if (lengthThird > maxLen) {
            maxLen = lengthThird;
        }

        if ( lengthFirst == maxLen) {
            System.out.println(word1);  
        }
        if (lengthSecond == maxLen) {
            System.out.println(word2);
        }
        if (lengthThird == maxLen) {
            System.out.println(word3);
        }

        //int maxLen = Math.max(Math.max(lengthFirst, lengthSecond), lengthThird);
    }

    public static boolean isLeapYear(int year) {
        // Sett inn kommentar hvis koden blir "rotete" ...
        // return year % 4 == 0;
        if (year % 4 == 0) { 
            if (year % 400 == 0) {
                return true;
            }

            if (year % 100 == 0) {
                return false;
            }
            return true;
        }
        return false;

        // Fra oppg.gjennomgang:
        // if (year % 4 == 0) {
        //     if (year % 100 == 0) {
        //         return year % 400 == 0;
        //     }

        //     else {
        //         return true;
        //     }
        // }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 == 0) {
            if (num > 0) {
                return true;
            }
            if (num < 0) {
                return false;
            }
            return true;
        }
        return false;
    }

    //return num % 2 == 0 && num >= 0;

}
