package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        //multiplesOfSevenUpTo(0);
        //multiplicationTable(2);
        System.out.println(crossSum(13));
    }

    public static void multiplesOfSevenUpTo(int n) {
        int num = 7;
        //while (num <= n) {
        while (true) {
            System.out.println(num);
            num += 7;

            if (num > n) {
                break;
            }
        }
        
        // Eller slik:
        // for (int i = 1; i <= n; i++) {
        //     if ( i % 7 == 0) {
        //         System.out.println(i);
        //     }
        // }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        // lager en variabel for summen
        int sum = 0;

        // kjører så lenge "num" ikke er 0
        while (num != 0) {
            // finner det siste sifferet i "num" og oppdaterer det i "sum"
            sum = sum + num % 10;

            // fjerner det siste sifferet
            num = num / 10;
        }

        // returnerer summen
        return sum;

        // Eller slik:
        // int result = 0;

        // while (num > 0) {
        //     result += num % 10;
        //     num /= 10;
        // }
        // return result;
    }
}